import { lazy, Suspense } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import { MarvelProvider } from 'contexts/MarvelContext';
import { PinProvider } from 'contexts/PinItemContext';
import BasicLayoutRoute from 'components/CustomRoutes/BasicRoute';
import LoaderSpinner from 'components/LoaderSpinner';
import './App.scss';

const CharacterDetails = lazy(() => import('pages/CharacterDetails'));
const PinnedItemsList = lazy(() => import('pages/PinnedItemsList'));
const CharactersList = lazy(() => import('./pages/CharactersList'));
const ComicDetails = lazy(() => import('./pages/ComicDetails'));
const StoryDetails = lazy(() => import('./pages/StoryDetails'));
const ComicList = lazy(() => import('./pages/ComicsList'));
const StoriesList = lazy(() => import('pages/StoriesList'));

function App(): JSX.Element {
  return (
    <div className="App">
      <MarvelProvider>
        <PinProvider>
          <Suspense fallback={<LoaderSpinner />}>
            <Switch>
              <BasicLayoutRoute path="/stories/:itemId" component={StoryDetails} />
              <BasicLayoutRoute path="/characters/:itemId" component={CharacterDetails} />
              <BasicLayoutRoute path="/comics/:itemId" component={ComicDetails} />
              <BasicLayoutRoute path="/comics" component={ComicList} />
              <BasicLayoutRoute path="/stories" component={StoriesList} />
              <BasicLayoutRoute path="/saved" component={PinnedItemsList} />
              <BasicLayoutRoute path="/characters" component={CharactersList} />
              <Route exact path="/">
                <Redirect to="/characters" />
              </Route>
            </Switch>
          </Suspense>
        </PinProvider>
      </MarvelProvider>
    </div>
  );
}

export default App;
