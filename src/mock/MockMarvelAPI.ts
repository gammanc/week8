import { rest } from 'msw';
import { setupServer } from 'msw/node';
import * as mockResponses from './MockResponses';

const baseUrl = 'https://gateway.marvel.com/v1/public';

const handlers = [
  rest.get(`${baseUrl}/characters`, (req, res, ctx) => {
    const params = req.url.searchParams;
    if (params.has('nameStartsWith') || params.has('comics') || params.has('stories')) {
      return res(
        ctx.status(200),
        ctx.json({
          ...mockResponses.characterByNameResponse,
        }),
      );
    }
    return res(
      ctx.status(200),
      ctx.json({
        ...mockResponses.charactersMockResponse,
      }),
    );
  }),

  rest.get(`${baseUrl}/characters/:id`, (req, res, ctx) => {
    return res(
      ctx.status(200),
      ctx.json({
        ...mockResponses.singleCharacterMockResponse,
      }),
    );
  }),

  rest.get(`${baseUrl}/comics`, (req, res, ctx) => {
    const params = req.url.searchParams;
    if (params.has('titleStartsWith') || params.has('format')) {
      return res(
        ctx.status(200),
        ctx.json({
          ...mockResponses.comicByFilterResponse,
        }),
      );
    }
    return res(
      ctx.status(200),
      ctx.json({
        ...mockResponses.comicsMockResponse,
      }),
    );
  }),

  rest.get(`${baseUrl}/comics/:id`, (req, res, ctx) => {
    return res(
      ctx.status(200),
      ctx.json({
        ...mockResponses.singleComicMockResponse,
      }),
    );
  }),

  rest.get(`${baseUrl}/stories`, (req, res, ctx) => {
    return res(
      ctx.status(200),
      ctx.json({
        ...mockResponses.storiesMockResponse,
      }),
    );
  }),

  rest.get(`${baseUrl}/stories/:id`, (req, res, ctx) => {
    return res(
      ctx.status(200),
      ctx.json({
        ...mockResponses.singleStoryMockResponse,
      }),
    );
  }),

  rest.get('*', (req, res, ctx) => {
    return res(
      ctx.status(500),
      ctx.json({ error: `Please add a handler for ${req.url.toString()}` }),
    );
  }),
];

const server = setupServer(...handlers);

beforeAll(() => server.listen());
afterAll(() => server.close());
afterEach(() => server.resetHandlers());

export { server, rest };
