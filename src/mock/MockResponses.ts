import { ok } from 'assert';
import { CharacterResponse, ComicResponse, StoryResponse } from 'interfaces/ResponseInterface';

const mockedThumbnail = {
  extension: '.jpg',
  path: 'http://i.annihil.us/u/prod/marvel/i/mg/3/40/4bb4680432f73',
};

const commonResponse = {
  code: 200,
  status: 'ok',
  copyright: 'some copyright',
};

const commonDataResponse = {
  offset: 0,
  limit: 4,
  total: 4,
  count: 20,
};

const commonDataResponseSingle = {
  offset: 0,
  limit: 4,
  total: 1,
  count: 1,
};

const mockedCharacters = [
  {
    id: 1,
    name: 'Black Widow',
    thumbnail: mockedThumbnail,
    description: 'the Black Widow description',
  },
  {
    id: 2,
    name: 'Thor',
    thumbnail: mockedThumbnail,
    description: 'the thor description',
  },
  {
    id: 3,
    name: 'Iron man',
    thumbnail: mockedThumbnail,
    description: 'the iron man description',
  },
  {
    id: 4,
    name: 'Captain America',
    thumbnail: mockedThumbnail,
    description: 'the Captain America description',
  },
];

const mockedComics = [
  {
    id: 10,
    title: 'Avengers Assemble',
    description: 'Avengers assemble comic description',
    pageCount: 100,
    thumbnail: mockedThumbnail,
    digitalId: 104,
  },
  {
    id: 11,
    title: 'Awesome comic #1',
    description: 'awesome comic description',
    pageCount: 100,
    thumbnail: mockedThumbnail,
    digitalId: 100,
  },
  {
    id: 12,
    title: 'Awesome comic #2',
    description: 'awesome comic 2 description',
    pageCount: 100,
    thumbnail: mockedThumbnail,
    digitalId: 101,
  },
  {
    id: 13,
    title: 'Incredible comic #1',
    description: 'Incredible comic description',
    pageCount: 100,
    thumbnail: mockedThumbnail,
    digitalId: 102,
  },
  {
    id: 14,
    title: 'Incredible comic #1',
    description: 'Incredible comic description',
    pageCount: 100,
    thumbnail: mockedThumbnail,
    digitalId: 103,
  },
];

const mockedStories = [
  { id: 21, title: 'Story #1', description: 'The story 1 description', thumbnail: {} },
  { id: 22, title: 'Story #2', description: 'The story 2 description', thumbnail: {} },
  { id: 23, title: 'Story #3', description: 'The story 3 description', thumbnail: {} },
  { id: 24, title: 'Story #4', description: 'The story 4 description', thumbnail: {} },
];

const charactersMockResponse: CharacterResponse = {
  ...commonResponse,
  data: {
    ...commonDataResponse,
    total: 10,
    results: mockedCharacters,
  },
};

const characterByNameResponse: CharacterResponse = {
  ...commonResponse,
  data: {
    ...commonDataResponseSingle,
    results: [mockedCharacters[2]],
  },
};

const comicsMockResponse: ComicResponse = {
  ...commonResponse,
  data: {
    ...commonDataResponse,
    results: mockedComics,
  },
};

const comicByFilterResponse: ComicResponse = {
  ...commonResponse,
  data: {
    ...commonDataResponseSingle,
    results: [mockedComics[0]],
  },
};

const storiesMockResponse: StoryResponse = {
  ...commonResponse,
  data: {
    offset: 0,
    limit: 4,
    total: 40,
    count: 4,
    results: mockedStories,
  },
};

const storiesByFilterResponse: StoryResponse = {
  ...commonResponse,
  data: {
    ...commonDataResponseSingle,
    results: [mockedStories[0]],
  },
};

const singleCharacterMockResponse: CharacterResponse = {
  ...commonResponse,
  data: {
    ...commonDataResponseSingle,
    results: [mockedCharacters[0]],
  },
};

const singleComicMockResponse: ComicResponse = {
  ...commonResponse,
  data: {
    ...commonDataResponseSingle,
    results: [mockedComics[0]],
  },
};

const singleStoryMockResponse: StoryResponse = {
  ...commonResponse,
  data: {
    ...commonDataResponseSingle,
    results: [mockedStories[0]],
  },
};

const bookmarkMock = [
  {
    resourceURL: '/characters/1',
    image: { extension: '.jpg', path: 'http://i.annihil.us/u/prod/marvel/i/mg/3/40/4bb4680432f73' },
    title: 'Black Widow',
    subtitle: '',
  },
];

export {
  charactersMockResponse,
  comicsMockResponse,
  storiesMockResponse,
  singleCharacterMockResponse,
  singleComicMockResponse,
  singleStoryMockResponse,
  characterByNameResponse,
  comicByFilterResponse,
  storiesByFilterResponse,
  bookmarkMock,
};
