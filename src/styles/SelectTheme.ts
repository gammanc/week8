import { Theme } from 'react-select';

export default (theme: Theme): Theme => ({
  ...theme,
  colors: {
    neutral0: '#202020',
    primary: '#ffffff',
  },
});
