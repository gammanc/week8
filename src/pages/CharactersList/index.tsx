import { useCallback, useEffect, useState } from 'react';
import { debounce } from 'lodash';
import AsyncSelect from 'react-select/async';
import { useMarvelDispatch, useMarvelState } from 'contexts/MarvelContext';
import { updateCharacters } from 'contexts/MarvelContextHelpers';
import SimpleList from 'components/SimpleList';
import TextInput from 'components/common/TextInput';
import Pagination, { Pager } from 'components/common/Pagination';
import { OptionsType, OptionTypeBase, ValueType } from 'react-select';
import { getComicsSuggestions, getStoriesSuggestions } from 'api/MarvelAPI';
import SelectTheme from 'styles/SelectTheme';
import styles from 'styles/lists.module.scss';

type SelectValue = ValueType<OptionTypeBase, false>;

function CharactersList(): JSX.Element {
  const dispatch = useMarvelDispatch();
  const state = useMarvelState();

  const [searchTerm, setSearchTerm] = useState('');
  const [currentPage, setCurrentPage] = useState(1);
  const [selectedComic, setSelectedComic] = useState<SelectValue>(null);
  const [selectedStory, setSelectedStory] = useState<SelectValue>(null);

  const debouncedRequest = useCallback(
    debounce((pageOffset: number, term: string, comicId: number, storyId: number) => {
      updateCharacters(dispatch, pageOffset, term, comicId, storyId);
    }, 500),
    [],
  );

  const debounceComicsSelect = useCallback(
    debounce((inputValue, callback) => {
      getComicsSuggestions(inputValue).then((response) =>
        callback(
          response.data!.data!.results.map((comic) => {
            return { label: comic.title, value: comic.id };
          }),
        ),
      );
    }, 1000),
    [],
  );

  useEffect(() => {
    updateCharacters(dispatch, 0, searchTerm, 0, 0);
  }, []);

  const loadComicsOptions = (
    inputValue: string,
    callback: (options: OptionsType<OptionTypeBase>) => void,
  ) => {
    debounceComicsSelect(inputValue, callback);
  };

  const loadStoryOptions = (
    inputValue: string,
    callback: (options: OptionsType<OptionTypeBase>) => void,
  ) => {
    getStoriesSuggestions().then((response) =>
      callback(
        response.data!.data!.results.map((story) => {
          return { label: story.title, value: story.id };
        }),
      ),
    );
  };

  const handleSearchTerm = (evt: React.ChangeEvent<HTMLInputElement>) => {
    setSearchTerm(evt.target.value);
    setCurrentPage(1);
    debouncedRequest(0, evt.target.value, selectedComic?.value, selectedStory?.value);
  };

  const handleComicChange = (value: SelectValue) => {
    setSelectedComic(value);
    debouncedRequest(0, searchTerm, value?.value, selectedStory?.value);
  };

  const handleStoryChange = (value: SelectValue) => {
    setSelectedStory(value);
    debouncedRequest(0, searchTerm, selectedComic?.value, value?.value);
  };

  const paginate = (pageNumber: number, pageInfo: Pager) => {
    setCurrentPage(pageNumber);
    debouncedRequest(pageInfo.startIndex, searchTerm, selectedComic?.value, selectedStory?.value);
  };

  const { characters } = state;
  const { data, loading } = characters!;

  return (
    <div className={styles.container}>
      <div className={styles.filters}>
        <TextInput
          id="search"
          type="search"
          placeholder="Search by name"
          value={searchTerm}
          onChange={handleSearchTerm}
          className={styles.searchBox}
        />
        <label className={styles.selectLabel} htmlFor="comic-select">
          Select comic
        </label>
        <AsyncSelect
          cacheOptions
          defaultOptions
          placeholder="Filter by comic..."
          loadOptions={loadComicsOptions}
          value={selectedComic}
          onChange={handleComicChange}
          theme={SelectTheme}
          className={styles.selectFilter}
          isClearable
          inputId="comic-select"
        />
        <label className={styles.selectLabel} htmlFor="story-select">
          Select story
        </label>
        <AsyncSelect
          cacheOptions
          defaultOptions
          placeholder="Filter by story..."
          isSearchable={false}
          loadOptions={loadStoryOptions}
          value={selectedStory}
          onChange={handleStoryChange}
          theme={SelectTheme}
          className={styles.selectFilter}
          isClearable
          inputId="story-select"
        />
      </div>
      <SimpleList items={data?.data?.results || []} loading={loading} />
      {data ? (
        <Pagination
          currentPage={currentPage}
          pageSize={20}
          totalItems={data!.data!.total}
          onPageChange={paginate}
        />
      ) : (
        <div />
      )}
    </div>
  );
}

export default CharactersList;
