import { useEffect } from 'react';
import { RouteComponentProps } from 'react-router-dom';
import { useMarvelState, useMarvelDispatch } from 'contexts/MarvelContext';
import { updateComicDetail } from 'contexts/MarvelContextHelpers';
import styles from 'styles/details.module.scss';
import { getImageURL } from 'interfaces/ResponseInterface';
import SimpleList from 'components/SimpleList';
import BookmarkButton from 'components/BookmarkButton';

interface TParams {
  itemId: string;
}

const ComicDetails = ({ match }: RouteComponentProps<TParams>): JSX.Element => {
  const { characters, comics, stories } = useMarvelState();
  const dispatch = useMarvelDispatch();

  useEffect(() => {
    updateComicDetail(dispatch, match.params.itemId);
  }, []);

  const comicInfo = comics.data?.data?.results[0];
  const resourceURL = `/comics/${comicInfo?.id}`;
  return (
    <div>
      {comicInfo ? (
        <div className={styles.postView}>
          <div className={styles.postImgContainer}>
            <img
              className={styles.postImg}
              src={getImageURL(comicInfo.thumbnail!, 'detail')}
              alt={comicInfo.title}
            />
          </div>
          <div className={styles.postDetail}>
            <div className={styles.titleContainer}>
              <h1 className={styles.title}>{comicInfo.title}</h1>
            </div>
            <h2 className={styles.subtitle} id="subtitle">
              {comicInfo.description}
            </h2>
            <p>
              Pages:
              {comicInfo.pageCount}
            </p>
            <h2>Creators</h2>
            <p>
              {comicInfo.creators?.items
                ?.map((creator) => `${creator.name} (${creator.role})`)
                ?.join(', ')}
            </p>
            <BookmarkButton
              resourceURL={resourceURL}
              title={comicInfo.title!}
              image={comicInfo.thumbnail!}
              data-testid="btn-bookmark"
            />
          </div>
          <div className={styles.postRelated}>
            <h2>Characters</h2>
            <SimpleList items={characters.data?.data?.results || []} loading={characters.loading} />
            <h2>Related stories</h2>
            <SimpleList items={stories.data?.data?.results || []} loading={stories.loading} />
          </div>
        </div>
      ) : null}
    </div>
  );
};

export default ComicDetails;
