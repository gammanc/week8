import { useEffect } from 'react';
import { RouteComponentProps } from 'react-router-dom';
import { useMarvelState, useMarvelDispatch } from 'contexts/MarvelContext';
import { updateStoryDetail } from 'contexts/MarvelContextHelpers';
import styles from 'styles/details.module.scss';
import SimpleList from 'components/SimpleList';
import BookmarkButton from 'components/BookmarkButton';

interface TParams {
  itemId: string;
}

const StoryDetails = ({ match }: RouteComponentProps<TParams>): JSX.Element => {
  const { characters, comics, stories } = useMarvelState();
  const dispatch = useMarvelDispatch();

  useEffect(() => {
    updateStoryDetail(dispatch, match.params.itemId);
  }, []);

  const storyInfo = stories.data?.data?.results[0];
  const resourceURL = `/stories/${storyInfo?.id}`;

  return (
    <div>
      {storyInfo ? (
        <div className={styles.postView}>
          <div className={styles.postDetail}>
            <div className={styles.titleContainer}>
              <h1 className={styles.title}>{storyInfo.title}</h1>
            </div>
            <h2 className={styles.subtitle} id="subtitle">
              {storyInfo.description}
            </h2>
            <BookmarkButton
              data-testid="btn-bookmark"
              resourceURL={resourceURL}
              title={storyInfo.title!}
              image={{}}
            />
          </div>
          <div className={styles.postRelated}>
            <h2>Characters related</h2>
            <SimpleList items={characters.data?.data?.results || []} loading={characters.loading} />

            <h2>Comics related</h2>
            <SimpleList items={comics.data?.data?.results || []} loading={comics.loading} />
          </div>
        </div>
      ) : null}
    </div>
  );
};

export default StoryDetails;
