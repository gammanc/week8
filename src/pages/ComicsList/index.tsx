import { useCallback, useEffect, useState } from 'react';
import { debounce } from 'lodash';
import Select, { OptionTypeBase, ValueType } from 'react-select';
import TextInput from 'components/common/TextInput';
import Pagination, { Pager } from 'components/common/Pagination';
import { useMarvelDispatch, useMarvelState } from 'contexts/MarvelContext';
import { updateComics } from 'contexts/MarvelContextHelpers';
import { Comic, ComicFormats, getComicFormats } from 'interfaces/ComicsInterfaces';
import SelectTheme from 'styles/SelectTheme';
import styles from 'styles/lists.module.scss';
import SimpleList from 'components/SimpleList';

type SelectValue = ValueType<OptionTypeBase, false>;

function ComicList(): JSX.Element {
  const dispatch = useMarvelDispatch();
  const state = useMarvelState();

  const [searchTerm, setSearchTerm] = useState('');
  const [currentPage, setCurrentPage] = useState(1);
  const [selectedComicFormat, setSelectedComicFormat] = useState<SelectValue>(null);

  const debouncedRequest = useCallback(
    debounce((pageOffset: number, term: string, format: ComicFormats | null) => {
      updateComics(dispatch, pageOffset, term, format);
    }, 500),
    [],
  );

  useEffect(() => {
    updateComics(dispatch, 0, searchTerm, null);
  }, []);

  const handleSearchTerm = (evt: React.ChangeEvent<HTMLInputElement>) => {
    setSearchTerm(evt.target.value);
    setCurrentPage(1);
    debouncedRequest(0, evt.target.value, null);
  };

  const handleComicFormatChange = (value: SelectValue) => {
    setSelectedComicFormat(value);
    debouncedRequest(0, searchTerm, value?.value);
  };

  const paginate = (pageNumber: number, pageInfo: Pager) => {
    setCurrentPage(pageNumber);
    debouncedRequest(pageInfo.startIndex, searchTerm, null);
  };

  const comicFormats = () => {
    return getComicFormats().map((comicFormat) => ({
      label: comicFormat.value,
      value: comicFormat.value,
    }));
  };

  const { comics } = state;
  const { data, loading } = comics!;

  return (
    <div className={styles.container}>
      <div className={styles.filters}>
        <TextInput
          id="search"
          type="search"
          placeholder="Search by title"
          value={searchTerm}
          onChange={handleSearchTerm}
          className={styles.searchBox}
        />
        <label className={styles.selectLabel} htmlFor="format-select">
          Select format
        </label>
        <Select
          placeholder="Filter by comic format..."
          isSearchable={false}
          options={comicFormats()}
          value={selectedComicFormat}
          onChange={handleComicFormatChange}
          theme={SelectTheme}
          className={styles.selectFilter}
          isClearable
          inputId="format-select"
        />
      </div>
      <SimpleList<Comic> items={data?.data?.results || []} loading={loading} />
      {data ? (
        <Pagination
          currentPage={currentPage}
          pageSize={20}
          totalItems={data!.data!.total}
          onPageChange={paginate}
        />
      ) : (
        <div />
      )}
    </div>
  );
}

export default ComicList;
