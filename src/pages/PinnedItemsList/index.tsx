import Swal from 'sweetalert2';
import { useHistory } from 'react-router-dom';
import { getImageURL } from 'interfaces/ResponseInterface';
import { usePinState, usePinDispatch, deletePin, deleteAllPins } from 'contexts/PinItemContext';
import CardGrid from 'components/CardGrid';
import Card from 'components/Card/Card';
import Button from 'components/common/Button';
import styles from 'styles/lists.module.scss';

function PinnedItemsList(): JSX.Element {
  const history = useHistory();

  const pins = usePinState();
  const dispatch = usePinDispatch();

  const confirmDelete = (resourceURL: string) => {
    Swal.fire({
      title: 'Do you want to delete this item?',
      confirmButtonText: 'Delete',
      confirmButtonColor: 'red',
      showCancelButton: true,
    }).then((result) => {
      if (result.isConfirmed) {
        deletePin(dispatch, resourceURL);
      }
    });
  };

  const confirmDeleteAll = () => {
    Swal.fire({
      title: 'Do you want to delete all items?',
      text: 'This action cannot be undone',
      confirmButtonText: 'Delete',
      confirmButtonColor: 'red',
      showCancelButton: true,
    }).then((result) => {
      if (result.isConfirmed) {
        deleteAllPins(dispatch);
      }
    });
  };

  return (
    <div className={styles.container}>
      <h1>Saved Elements</h1>
      {pins.length > 0 && (
        <Button onClick={confirmDeleteAll} fitContent>
          Delete all
        </Button>
      )}
      {pins.length ? (
        <CardGrid>
          {pins.map((pin) => (
            <Card
              key={pin.resourceURL}
              title={pin.title}
              subtitle={pin.subtitle}
              image={getImageURL(pin.image, 'standard_fantastic')}
            >
              <div className={styles.buttonsContainer}>
                <Button onClick={() => history.push(pin.resourceURL)}>More</Button>
                <Button
                  data-testid="btn-delete"
                  onClick={() => confirmDelete(pin.resourceURL)}
                  fitContent
                  color="red"
                >
                  <i className="far fa-trash-alt" />
                </Button>
              </div>
            </Card>
          ))}
        </CardGrid>
      ) : (
        <p>No elements to show</p>
      )}
    </div>
  );
}

export default PinnedItemsList;
