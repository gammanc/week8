import { useCallback, useEffect, useState } from 'react';
import { debounce } from 'lodash';
import Pagination, { Pager } from 'components/common/Pagination';
import { useMarvelDispatch, useMarvelState } from 'contexts/MarvelContext';
import { updateStories } from 'contexts/MarvelContextHelpers';
import styles from 'styles/lists.module.scss';
import { Story } from 'interfaces/StoryInterfaces';
import SimpleList from 'components/SimpleList';

function StoriesList(): JSX.Element {
  const dispatch = useMarvelDispatch();
  const state = useMarvelState();
  const [currentPage, setCurrentPage] = useState(1);

  const debouncedRequest = useCallback(
    debounce((pageOffset: number) => {
      updateStories(dispatch, pageOffset);
    }, 500),
    [],
  );

  useEffect(() => {
    updateStories(dispatch, 0);
  }, []);

  const paginate = (pageNumber: number, pageInfo: Pager) => {
    setCurrentPage(pageNumber);
    debouncedRequest(pageInfo.startIndex);
  };

  const { stories } = state;
  const { loading, data } = stories!;

  return (
    <div className={styles.container}>
      <SimpleList<Story> items={data?.data?.results || []} loading={loading} />
      {data ? (
        <Pagination
          currentPage={currentPage}
          pageSize={20}
          totalItems={data!.data!.total}
          onPageChange={paginate}
        />
      ) : (
        <div />
      )}
    </div>
  );
}

export default StoriesList;
