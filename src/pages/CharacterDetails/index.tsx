import { useEffect } from 'react';
import { RouteComponentProps } from 'react-router-dom';
import { useMarvelState, useMarvelDispatch } from 'contexts/MarvelContext';
import { updateCharacterDetail } from 'contexts/MarvelContextHelpers';
import styles from 'styles/details.module.scss';
import { getImageURL } from 'interfaces/ResponseInterface';
import SimpleList from 'components/SimpleList';
import BookmarkButton from 'components/BookmarkButton';

interface TParams {
  itemId: string;
}

const CharacterDetails = ({ match }: RouteComponentProps<TParams>): JSX.Element => {
  const { characters, comics, stories } = useMarvelState();
  const dispatch = useMarvelDispatch();

  useEffect(() => {
    updateCharacterDetail(dispatch, match.params.itemId);
    window.scrollTo(0, 0);
  }, []);

  const characterInfo = characters.data?.data?.results[0];

  const resourceURL = `/characters/${characterInfo?.id}`;

  return (
    <div>
      {characterInfo ? (
        <div className={styles.postView}>
          <div className={styles.postImgContainer}>
            <img
              className={styles.postImg}
              src={getImageURL(characterInfo.thumbnail, 'detail')}
              alt={characterInfo.name}
            />
          </div>
          <div className={styles.postDetail}>
            <div className={styles.titleContainer}>
              <h1 className={styles.title}>{characterInfo.name}</h1>
            </div>
            <h2 className={styles.subtitle} id="subtitle">
              {characterInfo.description}
            </h2>
            <BookmarkButton
              data-testid="btn-bookmark"
              resourceURL={resourceURL}
              title={characterInfo.name!}
              image={characterInfo.thumbnail}
            />
          </div>
          <div className={styles.postRelated}>
            <h2>Comics related</h2>
            <SimpleList items={comics.data?.data?.results || []} loading={comics.loading} />

            <h2>Stories</h2>
            <SimpleList items={stories.data?.data?.results || []} loading={stories.loading} />
          </div>
        </div>
      ) : null}
    </div>
  );
};

export default CharacterDetails;
