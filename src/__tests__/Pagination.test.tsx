import { fireEvent, render } from '@testing-library/react';
import Pagination from '../components/common/Pagination';

global.scrollTo = jest.fn();

it('change page number updates correctly', () => {
  const { getByText, getByTestId, rerender } = render(
    <Pagination currentPage={1} pageSize={3} totalItems={10} onPageChange={() => {}} />,
  );
  // pagination starts in page 1
  let label = getByText(/1 - 3 of 10/i);
  expect(label).toBeInTheDocument();

  // pagination is changed to page 2
  const nextBtn = getByTestId('btn-next');
  fireEvent.click(nextBtn);
  rerender(<Pagination currentPage={2} pageSize={3} totalItems={10} onPageChange={() => {}} />);

  label = getByText(/4 - 6 of 10/i);
  expect(label).toBeInTheDocument();

  // pagination is changed to page 1 again
  const prevBtn = getByTestId('btn-prev');
  fireEvent.click(prevBtn);
  rerender(<Pagination currentPage={1} pageSize={3} totalItems={10} onPageChange={() => {}} />);
  label = getByText(/1 - 3 of 10/i);
  expect(label).toBeInTheDocument();
});
