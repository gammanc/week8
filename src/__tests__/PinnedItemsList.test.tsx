import { render } from '@testing-library/react';
import { PinProvider } from 'contexts/PinItemContext';
import PinnedItemsList from 'pages/PinnedItemsList';
import { bookmarkMock } from 'mock/MockResponses';
import userEvent from '@testing-library/user-event';

beforeEach(() => {
  Object.defineProperty(window, 'localStorage', {
    value: {
      getItem: jest.fn(() => JSON.stringify(bookmarkMock)),
      setItem: jest.fn(() => null),
    },
    writable: true,
  });
});

it('bookmarks list loads correctly', async () => {
  const { getByText } = render(
    <PinProvider>
      <PinnedItemsList />
    </PinProvider>,
  );

  const cardList = await getByText(/saved elements/i);
  expect(cardList).toBeInTheDocument();
});

it('user can delete a bookmark', async () => {
  const { getByTestId, getByText, findByText } = render(
    <PinProvider>
      <PinnedItemsList />
    </PinProvider>,
  );

  const deleteButton = getByTestId('btn-delete');
  userEvent.click(deleteButton);

  const modalConfirmButton = getByText('Delete');
  userEvent.click(modalConfirmButton);

  const labelInfo = await findByText(/no elements to show/i);
  expect(labelInfo).toBeInTheDocument();
});

it('user can delete a bookmark', async () => {
  const { getByText, findByText } = render(
    <PinProvider>
      <PinnedItemsList />
    </PinProvider>,
  );

  const deleteAllButton = getByText('Delete all');
  userEvent.click(deleteAllButton);

  const modalConfirmButton = getByText('Delete');
  userEvent.click(modalConfirmButton);

  const labelInfo = await findByText(/no elements to show/i);
  expect(labelInfo).toBeInTheDocument();
});
