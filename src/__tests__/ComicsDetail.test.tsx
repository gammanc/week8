import { routerTestProps } from 'utils/testRouterProps';
import renderWithContexts from 'utils/testWrapper';
import { PinProvider } from 'contexts/PinItemContext';
import ComicDetails from '../pages/ComicDetails/index';
import userEvent from '@testing-library/user-event';
import { bookmarkMock } from 'mock/MockResponses';

it('comic details renders correctly', async () => {
  const routeComponentPropsMock = routerTestProps('/character/:itemId', { itemId: '1' });

  const { findAllByText } = renderWithContexts(
    <PinProvider>
      <ComicDetails {...routeComponentPropsMock} />
    </PinProvider>,
    {
      route: 'comics/10',
    },
  );

  const title = await findAllByText(/avengers/i);
  expect(title[0]).toBeInTheDocument();
});

it('can add a comic to bookmarks', async () => {
  const routeComponentPropsMock = routerTestProps('/character/:itemId', { itemId: '1' });

  const { findByTestId, findByText } = renderWithContexts(
    <PinProvider>
      <ComicDetails {...routeComponentPropsMock} />
    </PinProvider>,
    {
      route: 'comics/10',
    },
  );

  const bookmarkButton = await findByTestId('btn-bookmark');
  expect(bookmarkButton).toBeInTheDocument();

  userEvent.click(bookmarkButton);

  const label = await findByText(/saved/i);
  expect(label).toBeInTheDocument();

  const key = 'pins';
  const value = JSON.stringify(bookmarkMock);

  expect(window.localStorage.setItem).toHaveBeenCalledTimes(2);
});
