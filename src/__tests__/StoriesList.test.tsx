import { act, findByText } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import StoriesList from 'pages/StoriesList';
import renderWithContexts from 'utils/testWrapper';

it('stories list loads correctly', async () => {
  const { findAllByTestId, getByTestId } = renderWithContexts(<StoriesList />);

  const loader = getByTestId('loader');
  expect(loader).toBeInTheDocument();

  const cardList = await findAllByTestId('card');
  expect(cardList.length).toBe(4);
});

jest.useFakeTimers('modern');

it('can paginate stories', async () => {
  const { findByText, findByTestId } = renderWithContexts(<StoriesList />);

  const nextPageButton = await findByTestId('btn-next');

  await act(async () => {
    userEvent.click(nextPageButton);
    jest.advanceTimersByTime(500);
  });

  const paginationLabel = await findByText('21 - 40 of 40');
  expect(paginationLabel).toBeInTheDocument();
});
