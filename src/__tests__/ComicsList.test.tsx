import { act, screen } from '@testing-library/react';
import selectEvent from 'react-select-event';
import userEvent from '@testing-library/user-event';
import ComicsList from 'pages/ComicsList';
import renderWithContexts from 'utils/testWrapper';

it('comics list renders correctly', async () => {
  const { findAllByTestId, getByTestId } = renderWithContexts(<ComicsList />);

  const loader = getByTestId('loader');
  expect(loader).toBeInTheDocument();

  const cardList = await findAllByTestId('card');
  expect(cardList.length).toBe(5);
});

it('user can click on comic card', async () => {
  const { findAllByText } = renderWithContexts(<ComicsList />);

  const buttons = await findAllByText(/more/i);
  expect(buttons.length).toBe(5);

  userEvent.click(buttons[0]);
  expect(screen.getByTestId('location-display')).toHaveTextContent('/comics/10');
});

jest.useFakeTimers('modern');

it('user can search comics by title', async () => {
  const { findByPlaceholderText, queryByText, findByText } = renderWithContexts(<ComicsList />);

  const searchInput = await findByPlaceholderText(/search by title/i);

  await act(async () => {
    userEvent.type(searchInput, 'a');
    jest.advanceTimersByTime(500);
  });

  expect(await findByText(/avengers/i)).toBeInTheDocument();
  expect(queryByText(/awesome comic/i)).not.toBeInTheDocument();
});

it('user can filter by comic format', async () => {
  const { findByText, getByLabelText, queryByText } = renderWithContexts(<ComicsList />);

  const card = await findByText(/avengers/i);
  expect(card).toBeInTheDocument();

  const formatSelect = getByLabelText(/select format/i);

  await act(async () => {
    await selectEvent.select(formatSelect, 'Magazine');
    jest.advanceTimersByTime(500);
  });

  expect(await findByText(/avengers/i)).toBeInTheDocument();
  expect(queryByText(/awesome comic/i)).not.toBeInTheDocument();
});
