import CharacterDetails from 'pages/CharacterDetails';
import renderWithContexts from 'utils/testWrapper';
import { PinProvider } from 'contexts/PinItemContext';
import { routerTestProps } from 'utils/testRouterProps';
import userEvent from '@testing-library/user-event';
import { bookmarkMock } from 'mock/MockResponses';

it('character details renders correctly', async () => {
  const routeComponentPropsMock = routerTestProps('/character/:itemId', { itemId: '1' });

  const { findAllByText, debug } = renderWithContexts(
    <PinProvider>
      <CharacterDetails {...routeComponentPropsMock} />
    </PinProvider>,
    {
      route: 'characters/1',
    },
  );

  const title = await findAllByText(/black widow/i);
  expect(title[0]).toBeInTheDocument();
});

it('can add a character to bookmarks', async () => {
  const routeComponentPropsMock = routerTestProps('/character/:itemId', { itemId: '1' });

  const { findByTestId, findByText } = renderWithContexts(
    <PinProvider>
      <CharacterDetails {...routeComponentPropsMock} />
    </PinProvider>,
    {
      route: 'characters/1',
    },
  );

  const bookmarkButton = await findByTestId('btn-bookmark');
  expect(bookmarkButton).toBeInTheDocument();

  userEvent.click(bookmarkButton);

  const label = await findByText(/saved/i);
  expect(label).toBeInTheDocument();

  const key = 'pins';
  const value = JSON.stringify(bookmarkMock);

  //one for initialization, one for setting new pin
  expect(window.localStorage.setItem).toHaveBeenCalledTimes(2);
  expect(window.localStorage.setItem).toHaveBeenLastCalledWith(key, value);
});
