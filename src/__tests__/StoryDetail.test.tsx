import { routerTestProps } from 'utils/testRouterProps';
import { PinProvider } from 'contexts/PinItemContext';
import renderWithContexts from 'utils/testWrapper';
import StoryDetails from 'pages/StoryDetails';
import userEvent from '@testing-library/user-event';
import { bookmarkMock } from 'mock/MockResponses';

it('story details renders correctly', async () => {
  const routeComponentPropsMock = routerTestProps('/stories/:itemId', { itemId: '1' });

  const { findAllByText } = renderWithContexts(
    <PinProvider>
      <StoryDetails {...routeComponentPropsMock} />
    </PinProvider>,
    {
      route: 'stories/21',
    },
  );

  const title = await findAllByText(/story #1/i);
  expect(title[0]).toBeInTheDocument();
});

it('can add a story to bookmarks', async () => {
  const routeComponentPropsMock = routerTestProps('/character/:itemId', { itemId: '1' });

  const { findByTestId, findByText } = renderWithContexts(
    <PinProvider>
      <StoryDetails {...routeComponentPropsMock} />
    </PinProvider>,
    {
      route: 'comics/10',
    },
  );

  const bookmarkButton = await findByTestId('btn-bookmark');
  expect(bookmarkButton).toBeInTheDocument();

  userEvent.click(bookmarkButton);

  const label = await findByText(/saved/i);
  expect(label).toBeInTheDocument();

  const key = 'pins';
  const value = JSON.stringify(bookmarkMock);

  expect(window.localStorage.setItem).toHaveBeenCalledTimes(2);
});
