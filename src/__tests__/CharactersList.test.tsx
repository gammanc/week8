import { act, screen } from '@testing-library/react';
import selectEvent from 'react-select-event';
import userEvent from '@testing-library/user-event';
import CharactersList from 'pages/CharactersList/index';
import renderWithContexts from 'utils/testWrapper';

it('characters list loads correctly', async () => {
  const { findAllByTestId, getByTestId } = renderWithContexts(<CharactersList />);

  // display loader spinner
  const loader = getByTestId('loader');
  expect(loader).toBeInTheDocument();

  // 4 entries should return from msw
  const cardList = await findAllByTestId('card');
  expect(cardList.length).toBe(4);
});

it('user can click on character card', async () => {
  const { findAllByText } = renderWithContexts(<CharactersList />);

  const buttons = await findAllByText(/more/i);
  expect(buttons.length).toBe(4);

  userEvent.click(buttons[0]);
  expect(screen.getByTestId('location-display')).toHaveTextContent('/characters/1');
});

jest.useFakeTimers('modern');

it('user can type on search input', async () => {
  const { findByPlaceholderText, queryByText, findByText } = renderWithContexts(<CharactersList />);

  const searchInput = await findByPlaceholderText(/search by name/i);

  await act(async () => {
    userEvent.type(searchInput, 'i');
    jest.advanceTimersByTime(500);
  });

  expect(await findByText(/iron man/i)).toBeInTheDocument();
  expect(queryByText(/thor/i)).not.toBeInTheDocument();
});

it('user can filter by comic', async () => {
  const { findByText, getByLabelText, queryByText } = renderWithContexts(<CharactersList />);

  const card = await findByText(/black widow/i);
  expect(card).toBeInTheDocument();

  const comicSelect = getByLabelText(/select comic/i);
  selectEvent.openMenu(comicSelect);
  const comicsLabel = await findByText(/awesome comic #1/i);
  expect(comicsLabel).toBeInTheDocument();

  await act(async () => {
    await selectEvent.select(comicSelect, 'Awesome comic #1');
    jest.advanceTimersByTime(500);
  });

  expect(await findByText(/iron man/i)).toBeInTheDocument();
  expect(queryByText(/thor/i)).not.toBeInTheDocument();
});

it('user can filter by story', async () => {
  const { findByText, getByLabelText, debug, queryByText } = renderWithContexts(<CharactersList />);

  const card = await findByText(/black widow/i);
  expect(card).toBeInTheDocument();

  const storiesSelect = getByLabelText(/select story/i);
  selectEvent.openMenu(storiesSelect);
  const label = await findByText(/story #2/i);
  expect(label).toBeInTheDocument();

  await act(async () => {
    await selectEvent.select(storiesSelect, 'Story #2');
    jest.advanceTimersByTime(500);
  });

  expect(await findByText(/iron man/i)).toBeInTheDocument();
  expect(queryByText(/thor/i)).not.toBeInTheDocument();
});
