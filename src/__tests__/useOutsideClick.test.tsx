import Navbar from 'components/common/Navbar/Navbar';
import userEvent from '@testing-library/user-event';
import renderWithContexts from 'utils/testWrapper';
import { act, screen } from '@testing-library/react';

it('useOutsideClick hook works', () => {
  const { getByTestId, getByText } = renderWithContexts(
    <div>
      <Navbar />
      <button>Button</button>
    </div>,
  );

  const burguerButton = getByTestId('hamburguer-btn');
  userEvent.click(burguerButton);

  const navMenu = getByTestId('nav-menu');
  expect(navMenu.className).toBe('navMenu active');

  act(() => {
    const someButton = getByText(/button/i);
    userEvent.click(someButton);
  });

  expect(navMenu.className).toBe('navMenu');
});
