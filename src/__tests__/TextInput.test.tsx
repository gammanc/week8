import { render } from '@testing-library/react';
import TextInput from 'components/common/TextInput';

it('text input renders correctly', () => {
  const { getByLabelText } = render(
    <TextInput id="test-id" label="Label test" error={{ message: 'this an error' }} />,
  );
  const input = getByLabelText(/label test/i);
  expect(input).toHaveAttribute('id', 'test-id');
});
