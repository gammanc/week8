import { CharacterResponse, ComicResponse, StoryResponse } from 'interfaces/ResponseInterface';
import { COMICS_SUCCESS, COMICS_WAITING, COMICS_FAILED, ComicActions } from './ComicActions';
import {
  CHARACTERS_SUCCESS,
  CHARACTERS_FAILED,
  CHARACTERS_WAITING,
  CharacterActions,
} from './CharacterActions';
import { STORIES_FAILED, STORIES_SUCCESS, STORIES_WAITING, StoriesActions } from './StoryActions';

export function charactersWaitingAction(): CharacterActions {
  return {
    type: CHARACTERS_WAITING,
  };
}

export function charactersSuccessAction(characters: CharacterResponse): CharacterActions {
  return {
    type: CHARACTERS_SUCCESS,
    payload: {
      data: characters,
    },
  };
}

export function charactersErrorAction(error: string): CharacterActions {
  return {
    type: CHARACTERS_FAILED,
    payload: {
      error,
    },
  };
}

export function comicsWaitingAction(): ComicActions {
  return {
    type: COMICS_WAITING,
  };
}

export function comicsSuccessAction(comics: ComicResponse): ComicActions {
  return {
    type: COMICS_SUCCESS,
    payload: {
      data: comics,
    },
  };
}

export function comicsErrorAction(error: string): ComicActions {
  return {
    type: COMICS_FAILED,
    payload: {
      error,
    },
  };
}

export function storiesWaitingAction(): StoriesActions {
  return {
    type: STORIES_WAITING,
  };
}

export function storiesSuccessAction(stories: StoryResponse): StoriesActions {
  return {
    type: STORIES_SUCCESS,
    payload: {
      data: stories,
    },
  };
}

export function storiesErrorAction(error: string): StoriesActions {
  return {
    type: STORIES_FAILED,
    payload: {
      error,
    },
  };
}
