import PinnedItem from 'interfaces/PinInterface';

export enum PinActionTypes {
  PIN_ADD = 'PIN_ADD',
  PIN_DELETE = 'PIN_DELETE',
  PIN_DELETE_ALL = 'PIN_DELETE_ALL',
}

interface AddPinAction {
  type: PinActionTypes.PIN_ADD;
  payload: {
    pin: PinnedItem;
  };
}

interface DeletePinAction {
  type: PinActionTypes.PIN_DELETE;
  payload: {
    resourceURL: string;
  };
}

interface DeleteAllAction {
  type: PinActionTypes.PIN_DELETE_ALL;
}

export type PinActions = AddPinAction | DeletePinAction | DeleteAllAction;
