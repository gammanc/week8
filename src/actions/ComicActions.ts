import { ComicResponse } from 'interfaces/ResponseInterface';

export const COMICS_SUCCESS = 'COMICS_SUCCESS';
export const COMICS_FAILED = 'COMICS_FAILED';
export const COMICS_WAITING = 'COMICS_WAITING';

interface ComicsSuccessAction {
  type: typeof COMICS_SUCCESS;
  payload: {
    data: ComicResponse;
  };
}

interface ComicsFailAction {
  type: typeof COMICS_FAILED;
  payload: {
    error: string;
  };
}

interface ComicsWaitingAction {
  type: typeof COMICS_WAITING;
}

export type ComicActions = ComicsSuccessAction | ComicsFailAction | ComicsWaitingAction;
