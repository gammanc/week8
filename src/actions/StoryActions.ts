import { StoryResponse } from 'interfaces/ResponseInterface';

export const STORIES_SUCCESS = 'STORIES_SUCCESS';
export const STORIES_FAILED = 'STORIES_FAILED';
export const STORIES_WAITING = 'STORIES_WAITING';

interface StoriesSuccessAction {
  type: typeof STORIES_SUCCESS;
  payload: {
    data: StoryResponse;
  };
}

interface StoriesFailAction {
  type: typeof STORIES_FAILED;
  payload: {
    error: string;
  };
}

interface StoriesWaitingAction {
  type: typeof STORIES_WAITING;
}

export type StoriesActions = StoriesSuccessAction | StoriesFailAction | StoriesWaitingAction;
