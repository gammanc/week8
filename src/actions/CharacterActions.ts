import { CharacterResponse } from 'interfaces/ResponseInterface';

export const CHARACTERS_SUCCESS = 'CHARACTERS_SUCCESS';
export const CHARACTERS_FAILED = 'CHARACTERS_FAILED';
export const CHARACTERS_WAITING = 'CHARACTERS_WAITING';

interface CharactersSuccessAction {
  type: typeof CHARACTERS_SUCCESS;
  payload: {
    data: CharacterResponse;
  };
}

interface CharactersFailAction {
  type: typeof CHARACTERS_FAILED;
  payload: {
    error: string;
  };
}

interface CharactersWaitingAction {
  type: typeof CHARACTERS_WAITING;
}

export type CharacterActions =
  | CharactersSuccessAction
  | CharactersFailAction
  | CharactersWaitingAction;
