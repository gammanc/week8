import { render } from '@testing-library/react';
import { FunctionComponent, ReactElement, ReactNode } from 'react';
import { BrowserRouter, useLocation } from 'react-router-dom';
import { MarvelProvider } from 'contexts/MarvelContext';

interface Props {
  children: ReactNode;
}

const LocationDisplay = () => {
  const location = useLocation();
  return <div data-testid="location-display">{location.pathname}</div>;
};

const Wrapper = ({ children }: Props) => {
  return (
    <BrowserRouter>
      <MarvelProvider>{children}</MarvelProvider>
      <LocationDisplay />
    </BrowserRouter>
  );
};

const renderWithContexts = (ui: ReactElement, { route = '/' } = {}) => {
  window.history.pushState({}, 'Initial page', route);
  return render(ui, { wrapper: Wrapper as FunctionComponent });
};

export default renderWithContexts;
