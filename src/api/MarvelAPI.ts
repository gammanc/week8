import { AxiosResponse } from 'axios';
import { CharacterResponse, ComicResponse, StoryResponse } from 'interfaces/ResponseInterface';
import axiosInstance from './AxiosConfig';

export const getCharacters = (
  pageOffset: number,
  searchTerm: string,
  comicId: number,
  storyId: number,
): Promise<AxiosResponse<CharacterResponse>> => {
  let query = '';

  if (searchTerm) query += `&nameStartsWith=${searchTerm}`;
  if (comicId > 0) query += `&comics=${comicId}`;
  if (storyId > 0) query += `&stories=${storyId}`;
  if (pageOffset > 0) query += `&offset=${pageOffset}`;

  return axiosInstance.get(`/characters?${query}`);
};

export const getComics = (
  pageOffset: number,
  title: string,
  format?: string | null,
): Promise<AxiosResponse<ComicResponse>> => {
  let query = '';
  if (title) query += `&titleStartsWith=${title}`;
  if (format) query += `&format=${format}`;
  if (pageOffset > 0) query += `&offset=${pageOffset}`;

  return axiosInstance.get(`/comics?${query}`);
};

export const getComicsSuggestions = (title: string): Promise<AxiosResponse<ComicResponse>> => {
  let query = title ? `&titleStartsWith=${title}` : '';
  query += '&limit=5';
  return axiosInstance.get(`/comics?${query}`);
};

export const getStoriesSuggestions = (): Promise<AxiosResponse<ComicResponse>> => {
  return axiosInstance.get(`/stories?&limit=10`);
};

export const getStories = (pageOffset: number): Promise<AxiosResponse<StoryResponse>> => {
  let query = '';
  if (pageOffset > 0) query += `&offset=${pageOffset}`;
  return axiosInstance.get(`/stories?${query}`);
};

export const getCharacterDetails = (
  characterId: string,
): Promise<AxiosResponse<CharacterResponse>> => {
  return axiosInstance.get(`/characters/${characterId}`);
};

export const getComicDetails = (comicId: string): Promise<AxiosResponse<ComicResponse>> => {
  return axiosInstance.get(`/comics/${comicId}`);
};

export const getStoryDetails = (storyId: string): Promise<AxiosResponse<StoryResponse>> => {
  return axiosInstance.get(`/stories/${storyId}`);
};

type Entity = 'characters' | 'comics' | 'stories';
export const getResourcesOfEntity = (
  entity: Entity,
  resource: Entity,
  itemId: string,
): Promise<AxiosResponse<CharacterResponse | ComicResponse | StoryResponse>> => {
  return axiosInstance.get(`/${entity}/${itemId}/${resource}`);
};
