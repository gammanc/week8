import axios from 'axios';

const MARVEL_API_KEY = process.env.REACT_APP_MARVEL_KEY;

const axiosInstance = axios.create({
  baseURL: 'https://gateway.marvel.com/v1/public/',
  params: {
    apikey: MARVEL_API_KEY,
  },
  timeout: 20000,
});

export default axiosInstance;
