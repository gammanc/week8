import { ComicActions } from 'actions/ComicActions';
import { ComicResponse } from 'interfaces/ResponseInterface';
import { State } from 'interfaces/StateInterface';

export type ComicsState = State<ComicResponse>;

const comicsReducer = (state: ComicsState, action: ComicActions): ComicsState => {
  switch (action.type) {
    case 'COMICS_SUCCESS':
      return {
        loading: false,
        data: action.payload.data,
      };
    case 'COMICS_FAILED':
      return {
        loading: false,
        error: action.payload.error,
      };
    case 'COMICS_WAITING':
      return {
        loading: true,
        data: null,
        error: null,
      };
    default:
      return state;
  }
};

export default comicsReducer;
