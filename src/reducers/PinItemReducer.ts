import { PinActions, PinActionTypes } from 'actions/PinActions';
import PinItem from 'interfaces/PinInterface';

const pinItemReducer = (state: Array<PinItem>, action: PinActions): Array<PinItem> => {
  switch (action.type) {
    case PinActionTypes.PIN_ADD:
      if (state.findIndex((item) => item.resourceURL === action.payload.pin.resourceURL) < 0)
        return [...state, action.payload.pin];
      return state;
    case PinActionTypes.PIN_DELETE:
      return state.filter((pin) => pin.resourceURL !== action.payload.resourceURL);
    case PinActionTypes.PIN_DELETE_ALL:
      return [];
    default:
      return state;
  }
};

export default pinItemReducer;
