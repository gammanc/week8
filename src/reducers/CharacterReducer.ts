import { CharacterActions } from 'actions/CharacterActions';
import { CharacterResponse } from 'interfaces/ResponseInterface';
import { State } from 'interfaces/StateInterface';

export type CharactersState = State<CharacterResponse>;

const characterReducer = (state: CharactersState, action: CharacterActions): CharactersState => {
  switch (action.type) {
    case 'CHARACTERS_SUCCESS':
      return {
        loading: false,
        data: action.payload.data,
      };
    case 'CHARACTERS_FAILED':
      return {
        loading: false,
        error: action.payload.error,
      };
    case 'CHARACTERS_WAITING':
      return {
        loading: true,
        data: null,
        error: null,
      };
    default:
      return state;
  }
};

export default characterReducer;
