import { StoriesActions } from 'actions/StoryActions';
import { StoryResponse } from 'interfaces/ResponseInterface';
import { State } from 'interfaces/StateInterface';

export type StoryState = State<StoryResponse>;

const storiesReducer = (state: StoryState, action: StoriesActions): StoryState => {
  switch (action.type) {
    case 'STORIES_SUCCESS':
      return {
        loading: false,
        data: action.payload.data,
      };
    case 'STORIES_FAILED':
      return {
        loading: false,
        error: action.payload.error,
      };
    case 'STORIES_WAITING':
      return {
        loading: true,
        data: null,
        error: null,
      };
    default:
      return state;
  }
};

export default storiesReducer;
