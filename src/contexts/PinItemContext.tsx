import { createContext, Dispatch, useContext, ReactNode, useReducer, useEffect } from 'react';
import PinItem from 'interfaces/PinInterface';
import { PinActions, PinActionTypes } from 'actions/PinActions';
import pinItemReducer from 'reducers/PinItemReducer';

type PinProviderProps = {
  children: ReactNode;
};

const PINS_KEY = 'pins';

const PinStateContext = createContext<Array<PinItem>>([]);
const PinDispatchContext = createContext<Dispatch<PinActions> | null>(null);

export function usePinState(): Array<PinItem> {
  const context = useContext(PinStateContext);

  if (!context) {
    throw new Error('usePinState must be used within a PinProvider');
  }
  return context;
}

export function usePinDispatch(): Dispatch<PinActions> {
  const context = useContext(PinDispatchContext);

  if (!context) {
    throw new Error('usePinDispatch must be used within a PinProvider');
  }
  return context;
}

const getLocalData = () => {
  const pins = window.localStorage.getItem(PINS_KEY);
  return pins ? JSON.parse(pins) : [];
};

export const PinProvider = ({ children }: PinProviderProps): JSX.Element => {
  const [state, dispatch] = useReducer(pinItemReducer, [], getLocalData);

  useEffect(() => {
    window.localStorage.setItem(PINS_KEY, JSON.stringify(state));
  }, [state]);

  return (
    <PinStateContext.Provider value={state}>
      <PinDispatchContext.Provider value={dispatch}>{children}</PinDispatchContext.Provider>
    </PinStateContext.Provider>
  );
};

export function addPin(dispatch: Dispatch<PinActions>, pin: PinItem): void {
  dispatch({ type: PinActionTypes.PIN_ADD, payload: { pin } });
}

export function deletePin(dispatch: Dispatch<PinActions>, resourceURL: string): void {
  dispatch({ type: PinActionTypes.PIN_DELETE, payload: { resourceURL } });
}

export function deleteAllPins(dispatch: Dispatch<PinActions>): void {
  dispatch({ type: PinActionTypes.PIN_DELETE_ALL });
}

export function isPinned(resourceURL: string, pins: PinItem[]): boolean {
  const index = pins.findIndex((pin) => pin.resourceURL === resourceURL);
  return index >= 0;
}
