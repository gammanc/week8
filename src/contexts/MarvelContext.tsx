import { createContext, useContext, useReducer, ReactNode, Dispatch } from 'react';
import characterReducer, { CharactersState } from 'reducers/CharacterReducer';
import comicsReducer, { ComicsState } from 'reducers/ComicReducer';
import { CharacterActions } from 'actions/CharacterActions';
import { ComicActions } from 'actions/ComicActions';
import { StoriesActions } from 'actions/StoryActions';
import storiesReducer, { StoryState } from 'reducers/StoryReducer';

type MarvelProviderProps = {
  children: ReactNode;
};

type MainState = {
  characters: CharactersState;
  comics: ComicsState;
  stories: StoryState;
};

const MarvelStateContext = createContext<MainState | null>(null);
const MarvelDispatchContext = createContext<Dispatch<
  CharacterActions | ComicActions | StoriesActions
> | null>(null);

export function useMarvelState(): MainState {
  const context = useContext(MarvelStateContext);

  if (!context) {
    throw new Error('useMarvelState must be used within a MarvelProvider');
  }
  return context;
}

export function useMarvelDispatch(): Dispatch<CharacterActions | ComicActions | StoriesActions> {
  const context = useContext(MarvelDispatchContext);

  if (!context) {
    throw new Error('useMarvelDispatch must be used within a MarvelProvider');
  }
  return context;
}

const mainReducer = (
  initialState: MainState,
  action: CharacterActions | ComicActions | StoriesActions,
): MainState => ({
  characters: characterReducer(initialState.characters, action as CharacterActions),
  comics: comicsReducer(initialState.comics, action as ComicActions),
  stories: storiesReducer(initialState.stories, action as StoriesActions),
});

export const MarvelProvider = ({ children }: MarvelProviderProps): JSX.Element => {
  const [state, dispatch] = useReducer(mainReducer, {
    characters: { loading: true },
    comics: { loading: true },
    stories: { loading: true },
  });
  return (
    <MarvelStateContext.Provider value={state}>
      <MarvelDispatchContext.Provider value={dispatch}>{children}</MarvelDispatchContext.Provider>
    </MarvelStateContext.Provider>
  );
};
