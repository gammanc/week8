import { Dispatch } from 'react';
import { AxiosError, AxiosResponse } from 'axios';
import {
  getCharacterDetails,
  getStoryDetails,
  getComicDetails,
  getCharacters,
  getComics,
  getResourcesOfEntity,
  getStories,
} from 'api/MarvelAPI';
import { CharacterResponse, ComicResponse, StoryResponse } from 'interfaces/ResponseInterface';
import * as actionCreator from 'actions/ActionCreators';
import { ComicFormats } from 'interfaces/ComicsInterfaces';
import { CharacterActions } from 'actions/CharacterActions';
import { ComicActions } from 'actions/ComicActions';
import { StoriesActions } from 'actions/StoryActions';

type MarvelDispatch = Dispatch<CharacterActions | ComicActions | StoriesActions>;

function handleCharacterResponse(
  dispatch: Dispatch<CharacterActions>,
  response: AxiosResponse<CharacterResponse>,
) {
  const characterResponse: CharacterResponse = response.data;
  dispatch(actionCreator.charactersSuccessAction(characterResponse));
}

function handleComicResponse(
  dispatch: Dispatch<ComicActions>,
  response: AxiosResponse<ComicResponse>,
) {
  const comicResponse: ComicResponse = response.data;
  dispatch(actionCreator.comicsSuccessAction(comicResponse));
}

function handleStoryResponse(
  dispatch: Dispatch<StoriesActions>,
  response: AxiosResponse<StoryResponse>,
) {
  const storyResponse: StoryResponse = response.data;
  dispatch(actionCreator.storiesSuccessAction(storyResponse));
}

function setWaitingAll(dispatch: MarvelDispatch) {
  dispatch(actionCreator.charactersWaitingAction());
  dispatch(actionCreator.comicsWaitingAction());
  dispatch(actionCreator.storiesWaitingAction());
}

export function updateCharacters(
  dispatch: Dispatch<CharacterActions>,
  pageOffset: number,
  term: string,
  comicId: number,
  storyId: number,
): void {
  dispatch(actionCreator.charactersWaitingAction());
  getCharacters(pageOffset, term, comicId, storyId)
    .then((response) => handleCharacterResponse(dispatch, response))
    .catch((error: AxiosError) => {
      dispatch(actionCreator.charactersErrorAction(error.message));
    });
}

export function updateComics(
  dispatch: Dispatch<ComicActions>,
  pageOffset: number,
  term: string,
  format: ComicFormats | null,
): void {
  dispatch(actionCreator.comicsWaitingAction());
  getComics(pageOffset, term, format)
    .then((response) => handleComicResponse(dispatch, response))
    .catch((error: AxiosError) => {
      dispatch(actionCreator.comicsErrorAction(error.message));
    });
}

export function updateStories(dispatch: Dispatch<StoriesActions>, pageOffset: number): void {
  dispatch(actionCreator.storiesWaitingAction());
  getStories(pageOffset)
    .then((response) => handleStoryResponse(dispatch, response))
    .catch((error: AxiosError) => {
      dispatch(actionCreator.storiesErrorAction(error.message));
    });
}

export function updateCharacterDetail(dispatch: MarvelDispatch, characterId: string): void {
  setWaitingAll(dispatch);

  getCharacterDetails(characterId)
    .then((response) => handleCharacterResponse(dispatch, response))
    .catch((error: AxiosError) => {
      dispatch(actionCreator.charactersErrorAction(error.message));
    });

  getResourcesOfEntity('characters', 'comics', characterId)
    .then((response) => handleComicResponse(dispatch, response as AxiosResponse<ComicResponse>))
    .catch((error: AxiosError) => {
      dispatch(actionCreator.comicsErrorAction(error.message));
    });

  getResourcesOfEntity('characters', 'stories', characterId)
    .then((response) => handleStoryResponse(dispatch, response as AxiosResponse<StoryResponse>))
    .catch((error: AxiosError) => {
      dispatch(actionCreator.storiesErrorAction(error.message));
    });
}

export function updateComicDetail(dispatch: MarvelDispatch, comicId: string): void {
  setWaitingAll(dispatch);

  getComicDetails(comicId)
    .then((response) => handleComicResponse(dispatch, response))
    .catch((error: AxiosError) => {
      dispatch(actionCreator.comicsErrorAction(error.message));
    });

  getResourcesOfEntity('comics', 'characters', comicId)
    .then((response) =>
      handleCharacterResponse(dispatch, response as AxiosResponse<CharacterResponse>),
    )
    .catch((error: AxiosError) => {
      dispatch(actionCreator.comicsErrorAction(error.message));
    });

  getResourcesOfEntity('comics', 'stories', comicId)
    .then((response) => handleStoryResponse(dispatch, response as AxiosResponse<StoryResponse>))
    .catch((error: AxiosError) => {
      dispatch(actionCreator.storiesErrorAction(error.message));
    });
}

export function updateStoryDetail(dispatch: MarvelDispatch, storyId: string): void {
  setWaitingAll(dispatch);

  getStoryDetails(storyId)
    .then((response) => handleStoryResponse(dispatch, response))
    .catch((error: AxiosError) => {
      dispatch(actionCreator.comicsErrorAction(error.message));
    });

  getResourcesOfEntity('stories', 'characters', storyId)
    .then((response) =>
      handleCharacterResponse(dispatch, response as AxiosResponse<CharacterResponse>),
    )
    .catch((error: AxiosError) => {
      dispatch(actionCreator.comicsErrorAction(error.message));
    });

  getResourcesOfEntity('stories', 'comics', storyId)
    .then((response) => handleComicResponse(dispatch, response as AxiosResponse<ComicResponse>))
    .catch((error: AxiosError) => {
      dispatch(actionCreator.storiesErrorAction(error.message));
    });
}
