export interface State<T> {
  loading: boolean;
  error?: string | null;
  data?: T | null;
}
