import { Image, Url, ResourceList } from './ResponseInterface';

export enum ComicFormats {
  COMIC = 'Comic',
  MAGAZINE = 'Magazine',
  TRADE_PAPERBACK = 'Trade Paperback',
  HARDCOVER = 'Hardcover',
  DIGEST = 'Digest',
  GRAPHIC_NOVEL = 'Graphic Novel',
  DIGITAL_COMIC = 'Digital Comic',
  INFINITE_COMIC = 'Infinite Comic',
}

interface ComicDate {
  type?: string;
  date?: string;
}

interface ComicPrice {
  type?: string;
  price?: string;
}

export interface ComicSummary {
  resourceURI?: string;
  name?: string;
}

export interface CreatorSummary {
  resourceURI?: string;
  name?: string;
  role?: string;
}

export interface Comic {
  id?: number;
  digitalId?: number;
  title?: string;
  issueNumber?: string;
  variantDescription?: string;
  description?: string;
  modified?: string;
  isbn?: string;
  upc?: string;
  diamondCode?: string;
  ean?: string;
  issn?: string;
  format?: ComicFormats;
  pageCount?: number;
  resourceURI?: string;
  urls?: Array<Url>;
  variants?: Array<ComicSummary>;
  collections?: Array<ComicSummary>;
  dates?: Array<ComicDate>;
  prices?: Array<ComicPrice>;
  thumbnail?: Image;
  creators?: ResourceList<CreatorSummary>;
}

export function getComicFormats(): { id: string; value: ComicFormats }[] {
  return Object.entries(ComicFormats).map((format) => ({ id: format[0], value: format[1] }));
}

export function isComic(obj: unknown): obj is Comic {
  return (obj as Comic).digitalId !== undefined;
}
