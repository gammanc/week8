import { Image, ResourceList } from './ResponseInterface';
import { ComicSummary, CreatorSummary } from './ComicsInterfaces';
import { CharacterSummary } from './CharacterInterface';

export interface Story {
  id?: number;
  title?: string;
  description?: string;
  resourceURI?: string;
  type?: string;
  modified?: string;
  thumbnail?: Image;
  comics?: ResourceList<ComicSummary>;
  characters?: ResourceList<CharacterSummary>;
  creators?: ResourceList<CreatorSummary>;
  originalIssue?: ComicSummary;
}

export const isStory = (obj: unknown): obj is Story => {
  return (obj as Story).title !== undefined;
};
