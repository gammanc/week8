import { Character } from './CharacterInterface';
import { Comic } from './ComicsInterfaces';
import { Story } from './StoryInterfaces';

type ImageVariant =
  | 'standard_xlarge'
  | 'standard_fantastic'
  | 'standard_amazing'
  | 'detail'
  | 'landscape_incredible';
export interface ResponseDataContainer<T> {
  offset: number;
  limit: number;
  total: number;
  count: number;
  results: Array<T>;
}
export interface ResponseWrapper<T> {
  code?: number;
  status?: string;
  copyright?: string;
  attributionText?: string;
  attributionHTML?: string;
  data?: ResponseDataContainer<T>;
  etag?: string;
}

export type CharacterResponse = ResponseWrapper<Character>;
export type ComicResponse = ResponseWrapper<Comic>;
export type StoryResponse = ResponseWrapper<Story>;

export interface Url {
  url?: string;
  type?: string;
}

export interface Image {
  path?: string;
  extension?: string;
}

export interface ResourceList<T> {
  available?: number;
  returned?: number;
  collectionURI?: string;
  items?: Array<T>;
}

export function getImageURL(image: Image, variant: ImageVariant): string {
  if (image.path) return `${image.path}/${variant}.${image.extension}`;
  return 'http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available/standard_fantastic.jpg';
}
