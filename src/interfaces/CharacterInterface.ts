import { Image, Url } from './ResponseInterface';

export interface Character {
  id?: number;
  name?: string;
  description?: string;
  modified?: string;
  resourceURI?: string;
  urls?: Array<Url>;
  thumbnail: Image;
}

export interface CharacterSummary {
  resourceURI?: string;
  name?: string;
  role?: string;
}

export function isCharacter(obj: unknown): obj is Character {
  return (obj as Character).name !== undefined;
}
