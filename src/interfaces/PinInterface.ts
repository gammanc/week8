import { Image } from './ResponseInterface';

export default interface PinItem {
  resourceURL: string;
  title: string;
  subtitle?: string;
  image: Image;
}
