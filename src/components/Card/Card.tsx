import classes from './Card.module.scss';

interface Props {
  title?: string;
  subtitle?: string;
  image?: string;
  actionBtn?: JSX.Element | null;
  children?: JSX.Element;
}

const Card = ({ title, subtitle, image, actionBtn, children }: Props): JSX.Element => (
  <div className={classes.gridItem} data-testid="card">
    <div className={classes.card}>
      {image && <img className={classes.cardImg} src={image} alt={title} />}
      <div className={classes.cardContent}>
        {title && <h1 className={classes.cardHeader}>{title}</h1>}
        {subtitle && <p className={classes.cardText}>{subtitle}</p>}
        {actionBtn || null}
        {children}
      </div>
    </div>
  </div>
);

Card.defaultProps = {
  title: '',
  subtitle: '',
  image: '',
  actionBtn: null,
  children: null,
};

export default Card;
