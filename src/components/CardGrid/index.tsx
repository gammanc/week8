import { ReactNode } from 'react';
import styles from './CardGrid.module.scss';

type Props = {
  children: ReactNode;
};

const CardGrid = ({ children }: Props): JSX.Element => {
  return <div className={styles.cardGrid}>{children}</div>;
};

export default CardGrid;
