import { Route, RouteProps } from 'react-router-dom';
import BasicLayout from 'components/layouts/BasicLayout/BasicLayout';
import { ElementType } from 'react';

const BasicLayoutRoute = ({ component: Component, ...rest }: RouteProps): JSX.Element => {
  const RenderComponent = Component as ElementType;
  return (
    <Route
      {...rest}
      render={(props) => (
        <BasicLayout>
          <RenderComponent {...props} />
        </BasicLayout>
      )}
    />
  );
};

export default BasicLayoutRoute;
