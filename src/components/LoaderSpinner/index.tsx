import styles from './LoaderSpinner.module.scss';

const LoaderSpinner = (): JSX.Element => {
  return (
    <div className={styles.loaderContainer} data-testid="loader">
      <div className={styles.loader} />
    </div>
  );
};

export default LoaderSpinner;
