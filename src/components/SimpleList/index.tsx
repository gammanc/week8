import { useHistory } from 'react-router-dom';
import Card from 'components/Card/Card';
import CardGrid from 'components/CardGrid';
import Button from 'components/common/Button';
import { getImageURL } from 'interfaces/ResponseInterface';
import { isComic } from 'interfaces/ComicsInterfaces';
import { isCharacter } from 'interfaces/CharacterInterface';
import { isStory } from 'interfaces/StoryInterfaces';
import LoaderSpinner from 'components/LoaderSpinner';

interface Props<T> {
  items: Array<T>;
  loading: boolean;
}

function SimpleList<T>({ items, loading }: Props<T>): JSX.Element {
  const history = useHistory();

  function renderItem(item: T) {
    if (isCharacter(item)) {
      return (
        <Card
          key={item.id}
          title={item.name}
          image={getImageURL(item.thumbnail, 'standard_fantastic')}
        >
          <Button onClick={() => history.push(`/characters/${item.id}`)}>More</Button>
        </Card>
      );
    }
    if (isComic(item)) {
      return (
        <Card
          key={item.id}
          title={item.title}
          image={getImageURL(item.thumbnail!, 'standard_fantastic')}
        >
          <Button onClick={() => history.push(`/comics/${item.id}`)}>More</Button>
        </Card>
      );
    }
    if (isStory(item)) {
      return (
        <Card key={item.id} title={item.title} subtitle={item.description}>
          <Button onClick={() => history.push(`/stories/${item.id}`)}>More</Button>
        </Card>
      );
    }
    return null;
  }

  return (
    <div>
      {loading && <LoaderSpinner />}
      {items.length > 0 && <CardGrid>{items.map((item) => renderItem(item))}</CardGrid>}
      {!items.length && !loading && 'No elements'}
    </div>
  );
}

export default SimpleList;
