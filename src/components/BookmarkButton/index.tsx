import ToggleButton from 'components/common/ToggleButton';
import { usePinState, isPinned, usePinDispatch, addPin } from 'contexts/PinItemContext';
import { Image } from 'interfaces/ResponseInterface';

interface Props {
  resourceURL: string;
  image: Image;
  title: string;
  subtitle?: string;
}

const BookmarkButton = ({ resourceURL, image, title, subtitle, ...rest }: Props): JSX.Element => {
  const pins = usePinState();
  const dispatch = usePinDispatch();
  const pinned = isPinned(resourceURL, pins);

  return (
    <ToggleButton
      active={pinned}
      label={pinned ? 'Saved' : 'Add to bookmarks'}
      fitContent
      {...rest}
      onClick={() =>
        addPin(dispatch, {
          resourceURL,
          image,
          title,
          subtitle,
        })
      }
    />
  );
};

BookmarkButton.defaultProps = {
  subtitle: '',
};

export default BookmarkButton;
