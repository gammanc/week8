import Navbar from 'components/common/Navbar/Navbar';
import Footer from 'components/common/Footer/Footer';
import styles from './BasicLayout.module.scss';

type Props = {
  children: React.ReactNode;
};

const BasicLayout = (props: Props): JSX.Element => {
  const { children } = props;
  return (
    <div className={styles.main}>
      <Navbar />
      <div className={styles.mainContainer}>{children}</div>
      <Footer />
    </div>
  );
};

export default BasicLayout;
