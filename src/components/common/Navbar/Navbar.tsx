import { memo, useRef, useState } from 'react';
import { Link } from 'react-router-dom';
import useOutsideClick from 'hooks/useOutsideClick';
import classes from './Navbar.module.scss';

const Navbar = () => {
  const [clicked, setClicked] = useState(false);
  const hamburguerButton = useRef<HTMLDivElement>(null);

  const handleClick = () => {
    setClicked(!clicked);
  };

  useOutsideClick(hamburguerButton, () => setClicked(false));

  const menuIconClasses = ['fas', classes['fa-icon']];
  if (clicked) {
    menuIconClasses.push('fa-times');
  } else {
    menuIconClasses.push('fa-bars');
  }

  return (
    <nav className={classes.navbarItems}>
      <Link className={classes.navbarLogo} style={{ textDecoration: 'none' }} to="/">
        <h1>MARVEL</h1>
      </Link>
      <div
        ref={hamburguerButton}
        data-testid="hamburguer-btn"
        className={classes.menuIcon}
        onClick={handleClick}
      >
        <i className={menuIconClasses.join(' ')} />
      </div>
      <ul
        data-testid="nav-menu"
        className={clicked ? [classes.navMenu, classes.active].join(' ') : classes.navMenu}
      >
        <li>
          <Link className={classes['nav-links']} to="/">
            Characters
          </Link>
          <Link className={classes['nav-links']} to="/comics">
            Comics
          </Link>
          <Link className={classes['nav-links']} to="/stories">
            Stories
          </Link>
          <Link className={classes['nav-links']} to="/saved">
            <i className="fas fa-bookmark" />
            Saved
          </Link>
        </li>
      </ul>
    </nav>
  );
};

export default memo(Navbar);
