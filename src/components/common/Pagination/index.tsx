import { useEffect, useState } from 'react';
import Button from 'components/common/Button';
import styles from './Pagination.module.scss';

export interface Pager {
  totalPages: number;
  startIndex: number;
  endIndex: number;
  totalItems: number;
}

interface PaginationProps {
  currentPage: number;
  pageSize: number;
  totalItems: number;
  onPageChange: (pageNumber: number, pageInfo: Pager) => void;
}

function Pagination({
  currentPage,
  pageSize,
  totalItems,
  onPageChange,
}: PaginationProps): JSX.Element {
  const calculatePager = (currentPageNumber = 1, pageSizeNumber = 10): Pager => {
    const totalPages = Math.ceil(totalItems / pageSizeNumber);
    const startIndex = (currentPageNumber - 1) * pageSizeNumber;
    const endIndex = Math.min(startIndex + pageSizeNumber - 1, totalItems - 1);

    return {
      totalPages,
      startIndex,
      endIndex,
      totalItems,
    };
  };

  const [pager, setPager] = useState<Pager | null>(null);

  const setPage = (page: number) => {
    const newPager = calculatePager(page, pageSize);
    if (pager && (page < 1 || page > newPager.totalPages)) return;
    window.scrollTo(0, 0);
    setPager(newPager);
    onPageChange(page, newPager);
  };

  useEffect(() => {
    const initialPager = calculatePager(currentPage, pageSize);
    setPager(initialPager);
  }, []);

  return pager ? (
    <div className={styles.paginator}>
      {pager.startIndex < totalItems ? (
        <p className={styles.label}>
          {`${pager.startIndex + 1} - ${pager.endIndex + 1} of ${totalItems}`}
        </p>
      ) : (
        <p />
      )}

      <Button
        className={styles.paginatorBtn}
        data-testid="btn-prev"
        onClick={() => setPage(currentPage - 1)}
        fitContent
      >
        <i className="fas fa-chevron-left" />
      </Button>
      <Button
        className={styles.paginatorBtn}
        data-testid="btn-next"
        onClick={() => setPage(currentPage + 1)}
        fitContent
      >
        <i className="fas fa-chevron-right" />
      </Button>
    </div>
  ) : (
    <div />
  );
}

export default Pagination;
