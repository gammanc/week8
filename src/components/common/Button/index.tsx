import styles from './Button.module.scss';

type Props = {
  fitContent?: boolean;
  onClick?: (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void | null;
  children?: React.ReactNode;
  type?: 'button' | 'submit';
};

const Button = ({
  fitContent = false,
  onClick,
  children,
  type,
  ...rest
}: Props & React.HTMLProps<HTMLButtonElement>): JSX.Element => {
  const btnStyle = [styles.btn];
  if (fitContent) btnStyle.push(styles.fitContent);
  return (
    <button type={type} className={btnStyle.join(' ')} onClick={onClick} {...rest}>
      {children}
    </button>
  );
};

Button.defaultProps = {
  fitContent: false,
  onClick: null,
  children: null,
  type: 'button',
};

export default Button;
