import { memo } from 'react';
import styles from './Footer.module.scss';

const Footer = () => (
  <footer className={styles.footer}>
    <p>A.S. 2021</p>
    <p>Data provided by Marvel API</p>
  </footer>
);

export default memo(Footer);
