import styles from 'components/common/Button/Button.module.scss';

type Props = {
  fitContent?: boolean;
  onClick?: (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void | null;
  active: boolean;
  label?: string;
  type?: 'button';
};

const ToggleButton = ({
  fitContent = false,
  active,
  label,
  onClick,
  ...rest
}: Props & React.HTMLProps<HTMLButtonElement>): JSX.Element => {
  const btnStyle = [styles.btn];
  if (fitContent) btnStyle.push(styles.fitContent);
  return (
    <button type="button" className={btnStyle.join(' ')} onClick={onClick} {...rest}>
      <div className={styles.content}>
        {active ? <i className="fas fa-bookmark" /> : <i className="far fa-bookmark" />}
        <p>{label}</p>
      </div>
    </button>
  );
};

ToggleButton.defaultProps = {
  fitContent: false,
  onClick: null,
  label: '',
  type: 'button',
};

export default ToggleButton;
