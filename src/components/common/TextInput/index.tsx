import { InputHTMLAttributes } from 'react';
import classes from './TextInput.module.scss';

interface Props extends InputHTMLAttributes<HTMLInputElement> {
  id: string;
  label?: string;
  error?: { message?: string };
}

const TextInput = ({ id, label, error, ...rest }: Props): JSX.Element => {
  return (
    <div className={classes.formControl}>
      {label && <label htmlFor={id}>{label}</label>}
      <input id={id} {...rest} />
      <i className="fas fa-info-circle" />
      <small>{error && error.message}</small>
    </div>
  );
};

TextInput.defaultProps = {
  error: null,
  label: '',
};

export default TextInput;
